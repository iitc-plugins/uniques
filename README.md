Allow manual entry of portals visited/captured. It will try and guess which portals you have captured from COMM/ portal details, but this will not catch every case.

Use the highlighter 'Uniques' included in this plugin to show the uniques on the map. 

For a syncronization between multiple browsers or desktop/mobile you may use  the plugin '[Sync](https://iitc.modos189.ru/build/test/plugins/sync.user.js)' from [Ingress Intel Total Conversion <sup>Community Edition</sup>](https://iitc.modos189.ru/)