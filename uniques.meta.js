// ==UserScript==
// @id             iitc-plugin-uniques@3ch01c
// @name           IITC plugin: Uniques
// @category       Misc
// @version        0.2.4.20181031.195523
// @namespace      https://github.com/3ch01c/ingress-intel-total-conversion
// @updateURL      https://gitlab.com/iitc-plugins/uniques/raw/master/uniques.meta.js
// @downloadURL    https://gitlab.com/iitc-plugins/uniques/raw/master/uniques.user.js
// @description    Allow manual entry of portals visited/captured. It will try and guess which portals you have captured from COMM/ portal details, but this will not catch every case. Use the highlighter 'Uniques' included in this plugin to show the uniques on the map. For a syncronization between multiple browsers or desktop/mobile you may use  the plugin 'Sync' from Ingress Intel Total Conversion CE.
// @include        https://*.ingress.com/intel*
// @include        http://*.ingress.com/intel*
// @match          https://*.ingress.com/intel*
// @match          http://*.ingress.com/intel*
// @include        https://*.ingress.com/mission/*
// @include        http://*.ingress.com/mission/*
// @match          https://*.ingress.com/mission/*
// @match          http://*.ingress.com/mission/*
// @grant          none
// ==/UserScript==